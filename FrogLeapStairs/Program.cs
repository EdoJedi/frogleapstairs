﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FrogLeapStairs
{  
  class Program
  {
    #region Frog Leap Challenge 1
    public static void FrogLeapChallenge()
    {
      long stairNum = -1;
      while (stairNum < 0)
      {
        Console.WriteLine("Enter Number of staircase.");
        string input1 = Console.ReadLine();
        if (!long.TryParse(input1, out stairNum))
        {
          stairNum = -1;
          Console.Clear();
          Console.WriteLine("Staircase must be a number\n");
        }
        if (stairNum < 1)
        {
          stairNum = -1;
          Console.Clear();
          Console.WriteLine("Staircase steps must be greater than 0\n");
        }
      }
      long leapPower = -1;
      while (leapPower < 0)
      {
        Console.WriteLine("Enter Number of steps the frog can leap forward when it jumps.");
        string input2 = Console.ReadLine();
        if (!long.TryParse(input2, out leapPower))
        {
          leapPower = -1;
          Console.Clear();
          Console.WriteLine("Leap forward must be a number.\n");
        }
        if (leapPower < 1)
        {
          leapPower = -1;
          Console.Clear();
          Console.WriteLine("\nFrog leaping power must be higher than 1.\n");
        }
      }

      long frogDrop = -1;
      while (frogDrop < 0)
      {
        Console.WriteLine("Enter Number of steps the frog drop back down.");
        string input3 = Console.ReadLine();
        if (!long.TryParse(input3, out frogDrop))
        {
          frogDrop = -1;
          Console.Clear();
          Console.WriteLine("Frog dropping back down must be a number.\n");
        }
        if (leapPower <= frogDrop)
        {
          frogDrop = -1;
          Console.Clear();
          Console.WriteLine("\nFrog dropping back down must be a smaller number than leaping forward.\n");
        }
      }

      long position = 0;
      long leapNum = 0;
      while (position <= stairNum)
      {
        position += leapPower;
        leapNum++;
        if (position >= stairNum)
        {
          break;
        }
        position -= frogDrop;
      }

      Console.WriteLine("\nTotal number of jump to reach upper floor: " + leapNum + "\n");
    }
    #endregion

    #region FrogMove Challenge 2
    public static List<string> moveLists = new List<string>();
    /// <summary>
    /// unique pattern: f(n) = f(n-1) + f(n-2)
    /// </summary>
    /// <param name="strPattern"></param>
    /// <param name="n"></param>
    public static void UniquePatterns(string pattern, int n)
    {
      if (n == 0)
      {
        moveLists.Add(pattern);
      }
      else if (n > 0)
      {
        UniquePatterns(pattern + "-step", n - 1);
        UniquePatterns(pattern + "-jump", n - 2);
      }
    }
    public static void FrogMove()
    {
      int distance = 0;
      while (distance < 1)
      {
        Console.WriteLine("Enter inches to travel.");
        string input1 = Console.ReadLine();
        if (!int.TryParse(input1, out distance))
        {
          distance = 0;
          Console.Clear();
          Console.WriteLine("Inches travel must be a number.\n");
        }
        else if (distance < 1)
        {
          distance = 0;
          Console.Clear();
          Console.WriteLine("Inches travel must be more than 0.\n");
        }
        else
        {
          UniquePatterns("", distance);
          string formattedMoveLists = string.Empty;
          for(int i = 0; i < moveLists.Count; i++)
          {
            string[] pArr = moveLists[i].Substring(1, moveLists[i].Length-1).Split('-');
            pArr[0] = pArr[0].ToUpper();
            if (i == 0)
            {
              formattedMoveLists += string.Join('-', pArr);
            }
            else if (i == moveLists.Count - 1)
            {
              formattedMoveLists += " and " + string.Join('-', pArr);
            }
            else
            {
              formattedMoveLists += ", " + string.Join('-', pArr);
            }
          }
          formattedMoveLists += ".";
          moveLists.Clear();
          Console.WriteLine("Frog Moving Patterns: ");
          Console.WriteLine(formattedMoveLists + "\n");
        }
      }
    }

    #endregion

    #region Clock Inverse Challenge 3
    public static void ClockInverse()
    {
      bool isValid = false;
      while (!isValid)
      {
        Console.WriteLine("Enter clock display time.");
        string input = Console.ReadLine();
        if (input.Contains(':'))
        {
          List<string> time = input.Split(':').ToList();

          if (time.Count < 2)
          {
            Console.Clear();
            Console.WriteLine("Invalid time format\n");
          }
          else
          {
            int hour = 0;
            if (!int.TryParse(time[0], out hour))
            {
              Console.Clear();
              Console.WriteLine("Invalid hour\n");
            }
            else if ( hour < 1 || hour > 12)
            {
              Console.Clear();
              Console.WriteLine("Hour must be within 1 to 12\n");
            }
            else
            {
              int minute = -1;
              if (!int.TryParse(time[1], out minute))
              {
                Console.Clear();
                Console.WriteLine("Invalid hour\n");
              }
              else if (minute < 0 || minute > 59)
              {
                Console.Clear();
                Console.WriteLine("Minute must be within 0 to 59\n");
              }
              else
              {
                int totalminute = 720;
                if (hour == 12)
                  hour = 0;
                int totalInverse = totalminute - (hour * 60 + minute);
                int inverseHour = totalInverse/60;
                int inverseMinute = totalInverse % 60;
                if(inverseHour == 0)
                {
                  inverseHour = 12;
                }
                Console.WriteLine("Time: " + inverseHour.ToString("D2") + ":" + inverseMinute.ToString("D2"));
                isValid = true;
              }
            }
          }
        }
        else
        {
          Console.Clear();
          Console.WriteLine("Invalid time format\n");
          isValid = false;
        }
      }
    }
    #endregion

    static void Main(string[] args)
    {
      int choiceInput = -1;
      while (choiceInput < 0)
      {
        Console.WriteLine("Choose challenge to test");
        Console.WriteLine("Challenge 1 - Frog Leaping Stairs: 1");
        Console.WriteLine("Challenge 2 - Frog Move Pattern: 2");
        Console.WriteLine("Challenge 3 - Mirror Clock Time: 3");
        Console.WriteLine("Exit: 4");
        string cInput = Console.ReadLine();
        if (!int.TryParse(cInput, out choiceInput))
        {
          choiceInput = -1;
          Console.Clear();
          Console.WriteLine("Please Choose a number between 1~4.\n");
        }
        switch (choiceInput)
        {
          case (1):
            Console.Clear();
            FrogLeapChallenge();
            choiceInput = -1;
            break;
          case (2):
            Console.Clear();
            FrogMove();
            choiceInput = -1;
            break;
          case (3):
            Console.Clear();
            ClockInverse();
            choiceInput = -1;
            break;
          case (4):
            choiceInput = 5;
            break;
          default:
            Console.Clear();
            Console.WriteLine("Please Choose a number between 1~4.\n");
            choiceInput = -1;
            break;
        }
      }

    }
  }
}
